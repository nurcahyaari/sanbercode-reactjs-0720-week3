import React from 'react';

const buah = [
    {
        nama: 'Semangka',
        harga: 10000,
        berat: 3000
    },
    {
        nama: 'Semangka',
        harga: 10000,
        berat: 3000
    },
    {
        nama: 'Semangka',
        harga: 10000,
        berat: 3000
    },
]

const TableHargaBuah = (props) => {
    // const { buah } = props;
    return (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
            <h1>Table harga buah</h1>
            <table style={{border: '1px solid black'}}>
                <tr style={{ backgroundColor: 'grey' }}>
                    <th style={{width: 300}}>Nama</th>
                    <th style={{width: 200}}>Harga</th>
                    <th style={{width: 200}}>Berat</th>
                </tr>
                {
                    buah.map((item) => (
                        <tr style={{ backgroundColor: 'salmon' }}>
                            <td>{item.nama}</td>
                            <td>{item.harga}</td>
                            <td>{item.berat / 1000}kg</td>
                        </tr>
                    ))
                }
            </table>
        </div>
    )
};

export default TableHargaBuah;

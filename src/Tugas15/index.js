import React, { useState, useCallback, useContext } from 'react';

import { BuahContext } from '../buahcontext';

const TablepriceBuah = (props) => {
    const  [ buah, handleRemove, handleChangeBuah, color ] = useContext(BuahContext);
    
    const [id, setid] = useState('');
    const [name, setname] = useState('');
    const [price, setprice] = useState(0);
    const [weight, setweight] = useState(0);
    
    const setDetail = useCallback((item) => () => {
        setid(item.id);
        setname(item.name);
        setprice(item.price);
        setweight(item.weight)
    });

    const unsetDetail = () => {
        setid('');
        setname('');
        setprice(0);
        setweight(0)
    }

    const changeBuah = (e) => {
        e.preventDefault();
        handleChangeBuah({
            name,
            price,
            weight,
        }, id);
        unsetDetail();
    }

    return (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
            <h1>Table price buah</h1>
            <table style={{border: '1px solid black'}}>
                <thead style={{ backgroundColor: 'grey' }}>
                    <th style={{width: 300}}>name</th>
                    <th style={{width: 200}}>price</th>
                    <th style={{width: 200}}>weight</th>
                    <th>#</th>
                </thead>
                <tbody>
                {
                    buah.map((item) => (
                        <tr key={item.id} style={{ backgroundColor: color }}>
                            <td>{item.name}</td>
                            <td>{item.price}</td>
                            <td>{item.weight / 1000}kg</td>
                            <td>
                                <button onClick={setDetail(item)}>Edit</button>
                                <button onClick={() => handleRemove(item.id)}>Hapus</button>
                            </td>
                        </tr>
                    ))
                }
                </tbody>
            </table>
            <div style={{paddingTop: 30}}>
                <h2>Edit Buah</h2>
                <form>
                    <div>
                        <label>name</label>
                        <input type="text" value={name} onChange={(e) => setname(e.target.value)}/>
                    </div>
                    <div>
                        <label>price</label>
                        <input type="number" value={price} onChange={(e) => setprice(e.target.value)}/>
                    </div>
                    <div>
                        <label>weight</label>
                        <input type="number" value={weight} onChange={(e) => setweight(e.target.value)}/>
                    </div>
                    <div>
                        <input type="button" value="cancel" onClick={unsetDetail}/>
                        <input type="submit" value="Save" onClick={changeBuah}/>
                    </div>
                </form>
            </div>
        </div>
    )
};

export default TablepriceBuah;

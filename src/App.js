import React, { useState, useEffect } from 'react';
import axios from 'axios';

import Tugas11 from './Tugas11';
import Tugas12 from './Tugas12';
import Tugas13 from './Tugas13';
import Tugas14 from './Tugas14';
import Tugas15 from './Tugas15';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { BuahContext } from './buahcontext';

const Fetch = axios.create({
  baseURL: 'http://backendexample.sanbercloud.com/api/',
});

function App() {
  const [buah, setBuah] = useState([]);

  const [color, setColor] = useState('#4687f0');
  const firstInit = async () => {
    const res = await Fetch.get("fruits");
    const { data } = res;
    
    setBuah(data);
  }
  
  useEffect(() => {
    firstInit();
  }, []);

  const handleRemove = async (id) => {
    const res = await Fetch.delete(`fruits/${id}`);
    console.log(res);
    if (res.data === 'success') {
      const newBuah = buah.filter((item) => item.id !== id)
      setBuah(newBuah);
      return;
    }
    alert("Gagal hapus");
  };

  const handleChangeBuah = async (data, id = '') => {
    console.log(id);
    if (id !== '') {
      const res = await Fetch.put(`fruits/${id}`, {
        name: data.name,
        price: data.price,
        weight: data.weight,
      });
      const newBuah = buah.map((item) => {
        if (item.id === id) {
          item.name = data.name;
          item.price = data.price;
          item.weight = data.weight;    
        }
        return item;
      });
      console.log(newBuah)
      setBuah(newBuah);
      return;
    }

    const res = await Fetch.post('fruits', {
      name: data.name,
      price: data.price,
      weight: data.weight,
    });
    
    buah.push({
      id: buah.length + 1,
      ...data,
    });
    setBuah(buah);
    
  }

  return (
    <BuahContext.Provider
      value={[
        buah,
        handleRemove,
        handleChangeBuah,
        color,
      ]}
    >
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/tugas11">Tugas 11</Link>
              </li>
              <li>
                <Link to="/tugas12">Tugas 12</Link>
              </li>
              <li>
                <Link to="/tugas13">Tugas 13</Link>
              </li>
              <li>
                <Link to="/tugas14">Tugas 14</Link>
              </li>
              <li>
                <Link to="/tugas15">Tugas 15</Link>
              </li>
            </ul>
          </nav>

          {/* A <Switch> looks through its children <Route>s and
              renders the first one that matches the current URL. */}
          <Switch>
            <Route exact path="/">
              <>
                Home
              </>
            </Route>
            <Route path="/tugas11">
              <Tugas11 />
            </Route>
            <Route path="/tugas12">
              <Tugas12 />
            </Route>
            <Route path="/tugas13">
              <Tugas13 />
            </Route>
            <Route path="/tugas14">
              <Tugas14 />
            </Route>
            <Route path="/tugas15">
              <Tugas15 />
            </Route>
          </Switch>
        </div>
      </Router>
    </BuahContext.Provider>
  );
}

export default App;

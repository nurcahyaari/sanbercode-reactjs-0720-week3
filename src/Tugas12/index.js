import React, { useState, useEffect } from 'react';

const Timer = () => {
    const [countdown, setcountdown] = useState(100);
    const time = new Date().toLocaleTimeString([], { hour: '2-digit', minute: "2-digit", second: "2-digit" });

    const timecount = () => {
        setTimeout(() => {
            const count = countdown - 1;
            setcountdown(count);
        }, 100)
    }

    useEffect(() => {
        timecount();
        if (countdown === 0) {
            return () => {
                console.log('Hello')
                clearTimeout(timecount);
            }
        }
    }, [countdown, timecount]);
    return (
        <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
            <h4>{time}</h4>
            <h4>Hitung Mundur: {countdown}</h4>
        </div>
    )
}

export default Timer;

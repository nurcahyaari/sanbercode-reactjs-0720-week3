import React, { useState, useCallback } from 'react';

const TableHargaBuah = (props) => {
    const [id, setid] = useState('');
    const [nama, setnama] = useState('');
    const [harga, setharga] = useState(0);
    const [berat, setBerat] = useState(0);
    const [buah, setBuah]  = useState([
        {
            id: 1,
            nama: 'Semangka',
            harga: 10000,
            berat: 3000
        },
        {
            id: 2,
            nama: 'Semangka',
            harga: 10000,
            berat: 3000
        },
        {
            id: 3,
            nama: 'Semangka',
            harga: 10000,
            berat: 3000
        },
    ]);

    const handleRemove = async (id) => {
        const newBuah = buah.filter((item) => item.id !== id)
        setBuah(newBuah);
      };
    
      const handleChangeBuah = async (data, id = '') => {
        if (id !== '') {
          const newBuah = buah.map((item) => {
            if (item.id === id) {
              item.name = data.name;
              item.price = data.price;
              item.weight = data.weight;    
            }
            return item;
          });
          console.log(newBuah)
          setBuah(newBuah);
          return;
        }
        
        buah.push({
          id: buah.length + 1,
          ...data,
        });
        setBuah(buah);
        
      }
    
    const setDetail = useCallback((item) => () => {
        setid(item.id);
        setnama(item.nama);
        setharga(item.harga);
        setBerat(item.berat)
    });

    const unsetDetail = () => {
        setid('');
        setnama('');
        setharga(0);
        setBerat(0)
    }

    const changeBuah = (e) => {
        e.preventDefault();
        handleChangeBuah({
            nama,
            harga,
            berat,
        }, id);
        unsetDetail();
    }

    return (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
            <h1>Table harga buah</h1>
            <table style={{border: '1px solid black'}}>
                <thead style={{ backgroundColor: 'grey' }}>
                    <th style={{width: 300}}>Nama</th>
                    <th style={{width: 200}}>Harga</th>
                    <th style={{width: 200}}>Berat</th>
                    <th>#</th>
                </thead>
                <tbody>
                {
                    buah.map((item) => (
                        <tr key={item.id} style={{ backgroundColor: 'salmon' }}>
                            <td>{item.nama}</td>
                            <td>{item.harga}</td>
                            <td>{item.berat / 1000}kg</td>
                            <td>
                                <button onClick={setDetail(item)}>Edit</button>
                                <button onClick={() => handleRemove(item.id)}>Hapus</button>
                            </td>
                        </tr>
                    ))
                }
                </tbody>
            </table>
            <div style={{paddingTop: 30}}>
                <h2>Edit Buah</h2>
                <form>
                    <div>
                        <label>Nama</label>
                        <input type="text" value={nama} onChange={(e) => setnama(e.target.value)}/>
                    </div>
                    <div>
                        <label>Harga</label>
                        <input type="number" value={harga} onChange={(e) => setharga(e.target.value)}/>
                    </div>
                    <div>
                        <label>Berat</label>
                        <input type="number" value={berat} onChange={(e) => setBerat(e.target.value)}/>
                    </div>
                    <div>
                        <input type="button" value="cancel" onClick={unsetDetail}/>
                        <input type="submit" value="Save" onClick={changeBuah}/>
                    </div>
                </form>
            </div>
        </div>
    )
};

export default TableHargaBuah;

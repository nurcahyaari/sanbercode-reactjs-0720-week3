import React, { useState, useCallback, useEffect } from 'react';
import axios from 'axios';

const Fetch = axios.create({
    baseURL: 'http://backendexample.sanbercloud.com/api/',
});
const TablepriceBuah = (props) => {
    const [buah, setBuah] = useState([]);

  const firstInit = async () => {
    const res = await Fetch.get("fruits");
    const { data } = res;
    
    setBuah(data);
  }
  
  useEffect(() => {
    firstInit();
  }, []);

  const handleRemove = async (id) => {
    const res = await Fetch.delete(`fruits/${id}`);
    console.log(res);
    if (res.data === 'success') {
      const newBuah = buah.filter((item) => item.id !== id)
      setBuah(newBuah);
      return;
    }
    alert("Gagal hapus");
  };

  const handleChangeBuah = async (data, id = '') => {
    if (id !== '') {
      const res = await Fetch.put(`fruits/${id}`, {
        name: data.name,
        price: data.price,
        weight: data.weight,
      });
      const newBuah = buah.map((item) => {
        if (item.id === id) {
          item.name = data.name;
          item.price = data.price;
          item.weight = data.weight;    
        }
        return item;
      });
      console.log(newBuah)
      setBuah(newBuah);
      return;
    }

    const res = await Fetch.post('fruits', {
      name: data.name,
      price: data.price,
      weight: data.weight,
    });
    
    buah.push({
      id: buah.length + 1,
      ...data,
    });
    setBuah(buah);
    
  }

    const [id, setid] = useState('');
    const [name, setname] = useState('');
    const [price, setprice] = useState(0);
    const [weight, setweight] = useState(0);
    
    const setDetail = useCallback((item) => () => {
        setid(item.id);
        setname(item.name);
        setprice(item.price);
        setweight(item.weight)
    });

    const unsetDetail = () => {
        setid('');
        setname('');
        setprice(0);
        setweight(0)
    }

    const changeBuah = (e) => {
        e.preventDefault();
        handleChangeBuah({
            name,
            price,
            weight,
        }, id);
        unsetDetail();
    }

    return (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
            <h1>Table price buah</h1>
            <table style={{border: '1px solid black'}}>
                <thead style={{ backgroundColor: 'grey' }}>
                    <th style={{width: 300}}>name</th>
                    <th style={{width: 200}}>price</th>
                    <th style={{width: 200}}>weight</th>
                    <th>#</th>
                </thead>
                <tbody>
                {
                    buah.map((item) => (
                        <tr key={item.id} style={{ backgroundColor: 'salmon' }}>
                            <td>{item.name}</td>
                            <td>{item.price}</td>
                            <td>{item.weight / 1000}kg</td>
                            <td>
                                <button onClick={setDetail(item)}>Edit</button>
                                <button onClick={() => handleRemove(item.id)}>Hapus</button>
                            </td>
                        </tr>
                    ))
                }
                </tbody>
            </table>
            <div style={{paddingTop: 30}}>
                <h2>Edit Buah</h2>
                <form>
                    <div>
                        <label>name</label>
                        <input type="text" value={name} onChange={(e) => setname(e.target.value)}/>
                    </div>
                    <div>
                        <label>price</label>
                        <input type="number" value={price} onChange={(e) => setprice(e.target.value)}/>
                    </div>
                    <div>
                        <label>weight</label>
                        <input type="number" value={weight} onChange={(e) => setweight(e.target.value)}/>
                    </div>
                    <div>
                        <input type="button" value="cancel" onClick={unsetDetail}/>
                        <input type="submit" value="Save" onClick={changeBuah}/>
                    </div>
                </form>
            </div>
        </div>
    )
};

export default TablepriceBuah;
